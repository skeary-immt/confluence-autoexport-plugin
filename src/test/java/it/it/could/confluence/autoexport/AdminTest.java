package it.it.could.confluence.autoexport;

import org.apache.commons.io.FileUtils;

import com.atlassian.confluence.util.GeneralUtil;

import java.io.File;

public class AdminTest extends AbstractIntegrationTestCase {
    private File exportDir;
    private AdminHelper admin = new AdminHelper();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        File tmpDir = new File("target/it-temp").getAbsoluteFile();
        if (tmpDir.exists()) {
            FileUtils.cleanDirectory(tmpDir);
        }
        tmpDir.mkdirs();
        exportDir = new File(tmpDir, "exports");
        exportDir.mkdirs();

        admin.configurePlugin(this, exportDir);
    }

    @Override
    protected void tearDown() throws Exception {
        FileUtils.deleteDirectory(exportDir);
        dropEscalatedPrivileges();
        
        super.tearDown();
    }

    public void testLinkAvailable() {
        gotoPageWithEscalatedPrivileges("/admin/console.action");
        assertLinkPresentWithExactText("Auto Export");
    }

    public void testChangeConfiguration() {
        admin.deleteConfiguration(this);
        admin.configurePlugin(this, exportDir);
        setWorkingForm("configform");
        assertTextFieldEquals("rootPath", exportDir.getAbsolutePath());
        assertTextFieldEquals("userName", "admin");
        assertTextPresent("Confluence user:");
        assertSelectedOptionEquals("encoding", "UTF-8");
    }

    public void testEditTemplate() {
        admin.restoreTemplate(this, "ds");
        assertLinkNotPresentWithExactText("Restore Default");
        assertLinkNotPresentWithExactText("View Current");
        clickLinkWithText("Edit Template", 1);
        setWorkingForm("edittemplateform");
        String templateData = getElementTextByXPath("//textarea[@name='data']");
        assertFalse(templateData.contains("asdf"));
        setTextField("data", templateData.replace("</body>", "asdf</body>"));
        submit("save");
        assertTextPresent("saved successfully");
        admin.gotoConfiguration(this);
        assertLinkPresentWithExactText("Restore Default");
        assertLinkPresentWithExactText("View Current");
        clickLinkWithExactText("View Current");
        assertTextPresent("asdf");
    }

    public void testXSSVulnerabilityWhenSpaceNameHasJavaScript()
    {
    	String spaceName = "Ja<script>alert('Error!!!');</script>;va";
    	String spaceKey = "JAVA";
    	
    	createSpace(spaceName, spaceKey);

    	admin.gotoConfiguration(this);

        assertTextPresent("Template for \"" + spaceName + "\" space:");
    
    	deleteSpace(spaceKey);
    }

    public void testAllSpaceIsAlwaysSelectedToExport()
    {
        admin.gotoConfiguration(this);
    	
    	assertEquals("selected", getElementAttributByXPath("//form//div//select[@name='spaces']/option[1]", "selected"));
    }
}
