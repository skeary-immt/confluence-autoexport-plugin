package it.it.could.confluence.autoexport;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.JWebUnitConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.TesterConfiguration;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;

import com.atlassian.confluence.util.GeneralUtil;
import junit.framework.Assert;

import java.io.IOException;
import java.util.Properties;

public class AbstractIntegrationTestCase extends AbstractConfluencePluginWebTestCase
{
    protected String testSpaceKey;
    protected String testSpaceName;
    
    protected void createTestSpace()
    {
        createSpace(testSpaceName = "Test Space", testSpaceKey = "tst");

        if (Long.parseLong(GeneralUtil.getBuildNumber()) >= 4000)
        {
            gotoPageWithEscalatedPrivileges("/spaces/choosetheme.action?key=" + testSpaceKey);
            clickElementByXPath("//input[@id='themeKey.com.atlassian.confluence.plugins.doctheme:documentation']");
            submit("Confirm");
        }
    }
    
    protected void createSpace(String spaceName, String spaceKey)
    {
        final SpaceHelper spaceHelper = getSpaceHelper();
    
        spaceHelper.setKey(spaceKey);
        spaceHelper.setName(spaceName);
        assertTrue(spaceHelper.create());
    }
    
    @Override
    protected JWebUnitConfluenceWebTester createConfluenceWebTester()
    {
        try
        {
            Properties props = new Properties();
            props.setProperty("confluence.webapp.port", System.getProperty("http.port", "1990"));
            props.setProperty("confluence.webapp.context.path", System.getProperty("context.path", "/confluence"));
            return new JWebUnitConfluenceWebTester(new TesterConfiguration(props));
        }
        catch (IOException ioe)
        {
            Assert.fail("Unable to create tester: " + ioe.getMessage());
            return null;
        }
        //tester.getTestContext().setBaseUrl(tester.getBaseUrl());
        //tester.setScriptingEnabled(false);

    }

    protected void deleteSpace(String spaceKey)
    {
        SpaceHelper spaceHelper = getSpaceHelper(spaceKey);
        assertTrue(spaceHelper.delete());
    }
}
