package it.it.could.confluence.autoexport;

import java.io.File;
import java.io.IOException;

public class SpaceAdminExportTestCase extends AbstractIntegrationTestCase
{
    @Override
    protected void setUp() throws Exception 
    {
        super.setUp();

        createTestSpace();
    }

    @Override
    protected void tearDown() throws Exception 
    {
        deleteSpace(testSpaceKey);
        
        super.tearDown();
    }
    
    public void testAutoExportLinkIsPresentInSpaceAdminMenu()
    {
        gotoPage("/spaces/editspace.action?key=" + testSpaceKey);
        assertElementPresentByXPath("//div[@id='space-admin-menu']//ul//li//a[@title='Auto Export']");
    }
    
    public void testAutoExportUIOnlyExportsCurrentSpace()
    {
        gotoPage("/spaces/editspace.action?key=" + testSpaceKey);
        clickLinkWithExactText("Auto Export");
        
        File folder = null;
        try 
        {
            folder = File.createTempFile(testSpaceKey, "");
            if (folder.exists())
                assertTrue(folder.delete());
            assertTrue(folder.mkdir());
            setTextField("rootPath", folder.getPath());
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
        
        clickElementByXPath("//form[@name='configform']//input[@name='saveConfig']");
        clickLinkWithExactText("Auto Export");
        
        assertElementPresentByXPath("//div[@id='content']//form//table//select[@name='spaces']//option[@value='" + testSpaceKey + "']");
        assertElementNotPresentByXPath("//div[@id='content']//form//table//select[@name='spaces']//option[2]");
        
        assertEquals("Template for \"" + testSpaceName + "\" space:", getElementTextByXPath("//div[@id='space-admin-body']//div[@align='center']//table//tr[3]//tbody//tr//td//label['label']"));
        assertElementNotPresentByXPath("//div[@id='space-admin-body']//div[@align='center']//table//tr[3]//tbody//tr[2]//tbody//tr//td//label['label']");
        
        assertTrue(folder.delete());
    }
}
