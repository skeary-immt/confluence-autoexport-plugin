package com.it.could.util.location;

import org.mockito.MockitoAnnotations;

import it.could.util.location.Authority;
import junit.framework.TestCase;

public class AuthorityTestCase extends TestCase
{
	private Authority authority;
	
	private String user = "admin";
	private String pass = "adminpass";
	private String host = "host-1";
	private int port = 1000;

	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		authority = new Authority(user, pass, host, port);
	}
	
	public void testOverwriteTheUserDetailsOfThisInstanceWithThoseOfTheSpecifiedAuthority()
	{
		String newUser = "philip";
		String newPass = "philippass";
		String newHost = "PhilipHost";
		int newPort = 1001;
		
		Authority newAuthority = new Authority(newUser, newPass, newHost, newPort);
		
		Authority resultAuthority = authority.merge(newAuthority);
		
		assertEquals(newUser, resultAuthority.getUser());
		assertEquals(newPass, resultAuthority.getPass());
		assertEquals(newHost, resultAuthority.getHost());
		assertEquals(newPort, resultAuthority.getPort());
		assertEquals(newUser + ":" + newPass + "@" + newHost + ":" + newPort, resultAuthority.toString());
	}
}
