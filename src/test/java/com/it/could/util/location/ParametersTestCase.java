package com.it.could.util.location;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import it.could.util.location.Parameter;
import it.could.util.location.Parameters;
import junit.framework.TestCase;

public class ParametersTestCase extends TestCase
{
	private Parameters parameters;
	
	private Parameter parameter;
	
	private List<Parameter> parametersList;
	private String parameterName = "LBL";
	private String parameterValue = "First Name";
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		parameter = new Parameter(parameterName, parameterName)
		{
			@Override
			public String getValue() 
			{
				return parameterValue;
			}
		};
		
		parametersList = new ArrayList<Parameter>();
		parametersList.add(parameter);
		
		parameters = new Parameters(parametersList);
	}
	
	public void testCreateNewParameterFromAListOfParameter()
	{
		String newParameterName = "LBL";
		String newParameterValue = "FirstName";
		
		Parameter newParameter = new Parameter(newParameterName, newParameterValue);
		
		List<Parameter> result = parameters.create(Arrays.asList(newParameter));
		
		assertEquals(1, result.size());
		
		Parameter parameterResult = result.get(0);
		
		assertEquals(newParameterName, parameterResult.getName());
		assertEquals(newParameterValue, parameterResult.getValue());
	}
	
	public void testParseAStringIntoAParameterWithDefaultEncoding() throws UnsupportedEncodingException
	{
		String parametersString = "This is an example string";
		
		Parameters parametersResult = parameters.parse(parametersString);
		
		assertEquals(1, parametersResult.size());
		assertEquals(parametersString, URLDecoder.decode(parametersResult.toArray()[0].toString(), "UTF-8"));
	}
}
