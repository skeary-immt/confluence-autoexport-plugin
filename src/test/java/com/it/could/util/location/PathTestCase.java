package com.it.could.util.location;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.mockito.MockitoAnnotations;

import it.could.util.encoding.Encodings;
import it.could.util.location.Path;
import junit.framework.TestCase;

public class PathTestCase extends TestCase
{
	private Path path;
	
	private List<Object> elements;
	private boolean absolute = true;
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		elements = new ArrayList<Object>();
		
		path = new Path(elements, absolute);
	}
	
	public void testParseTheSpecifiedLinkIntoAPathWithDefaultEncoding()
	{
		String filename = "page.txt";
		String protocol = "http";
		String link = "localhost/confluence/";
		String pathString = protocol + "://" + "localhost/confluence/" + filename;
		
		Path pathResult = path.resolve(pathString);
		
		assertEquals(filename, pathResult.filename());
		assertEquals("/" + protocol + ":/" + link + filename, pathResult.toString());
	}
	
	public void testParseTheSpecifiedLinkIntoAPathWithCustomEncoding() throws UnsupportedEncodingException
	{
		String filename = "backup.zip";
		String protocol = "https";
		String confluenceLink = "confluence<";
		String remoteHostLink = "remotehost";
		String pathString = protocol + "://" + remoteHostLink + "/" + confluenceLink + "/" + filename;
		String encoding = Encodings.get("ISO-8859-1");
		
		Path pathResult = path.resolve(pathString, encoding);
		
		assertEquals(filename, pathResult.filename());
		assertEquals("/" + protocol + ":/" + remoteHostLink + "/" + URLEncoder.encode(confluenceLink, encoding) + "/" +
				filename, pathResult.toString());
	}
	
	public void testParseTheSpecifiedStringIntoAPathWithDefaultEncoding() throws UnsupportedEncodingException
	{
		String text = "This is th\\u138 titl\\u138";
		
		Path pathResult = path.relativize(text);
		
		assertEquals(text, pathResult.filename());
		assertEquals(text, URLDecoder.decode(pathResult.toString(), "UTF-8"));
	}
	
	public void testParseTheSpecifiedStringIntoAPathWithCustomEncoding() throws UnsupportedEncodingException
	{
		String text = "P\\u148w\\u138r\\u138d By \\u142tl\\u160ssian C\\u148nflu\\u138nc\\u138";
		String encoding = Encodings.get("ISO-8859-1");
		
		Path pathResult = path.relativize(text, encoding);
		
		assertEquals(text, pathResult.filename());
		assertEquals(text, URLDecoder.decode(pathResult.toString(), "UTF-8"));
	}
}
