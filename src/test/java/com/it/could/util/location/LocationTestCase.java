package com.it.could.util.location;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import it.could.util.location.Location;
import junit.framework.TestCase;

public class LocationTestCase extends TestCase
{
	private Location location;
	
	@Mock private Location mockLocation;
	
	private String scheme;
	private String opaquePart = "";
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		scheme = new String(Location.SCHEME_START);
	}
	
	public void testCheckIfTheLocationIsAuthoritativeOrNotWhenOpaqueIsFalseAndIsAbsolute()
	{
		location = new Location(scheme, opaquePart)
		{
			@Override
			public boolean isOpaque() 
			{
				return false;
			}
		};
		
		when(mockLocation.isOpaque()).thenReturn(false);
		when(mockLocation.isAbsolute()).thenReturn(true);
		
		assertFalse(location.isAuthoritative(mockLocation));
	}
}
