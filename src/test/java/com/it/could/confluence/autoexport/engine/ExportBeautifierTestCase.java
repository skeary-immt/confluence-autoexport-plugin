package com.it.could.confluence.autoexport.engine;

import java.io.File;
import java.io.IOException;

import com.atlassian.confluence.core.ContentEntityManager;
import org.apache.commons.io.FileUtils;
import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XNIException;
import org.apache.xerces.xni.parser.XMLDocumentFilter;
import org.cyberneko.html.parsers.SAXParser;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import org.xml.sax.SAXException;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;

import it.could.confluence.autoexport.ConfigurationManager;
import it.could.confluence.autoexport.LocationManager;
import it.could.confluence.autoexport.engine.ExportBeautifier;
import it.could.util.location.Location;
import junit.framework.TestCase;

public class ExportBeautifierTestCase extends TestCase
{
	private ExportBeautifier beautifier;

	@Mock private ContainerContext containerContext;
	@Mock private ConfigurationManager configurationManager;
	@Mock private PageManager pageManager;
    @Mock private ContentEntityManager contentEntityManager;
	@Mock private SpaceManager spaceManager;
	@Mock private LocationManager locationManager;
	@Mock private Location exportUrl;

	private Page demoPage;

	private String encoding = "UTF-8";
	private String confluenceBase = "/confluence";
	private String urlPath = "/demopage";
	
	private SAXParser parser;
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		ContainerManager.getInstance().setContainerContext(containerContext);
		
		parser = new SAXParser();
		
		demoPage = new Page()
		{
			@Override
			public String getUrlPath() {
				return urlPath;
			}
		};
		
		when(locationManager.getLocation(demoPage)).thenReturn(exportUrl);
		when(configurationManager.getEncoding()).thenReturn(encoding);
		when(configurationManager.getConfluenceUrl()).thenReturn(confluenceBase);
		
		beautifier = new ExportBeautifier(demoPage, configurationManager, pageManager, spaceManager, locationManager, contentEntityManager)
		{
			@Override
			protected SAXParser getSAXParser() 
			{
				return parser;
			}

			@Override
			public void emptyElement(QName name, XMLAttributes atts,
					Augmentations augs) throws XNIException {
				// Do nothing
			}
		};
	}
	
	public void testBeautifyAndFixupTheLinksInTheHTMLContentAndWriteToFile() throws SAXException, IOException
	{
		String htmlContent = "<html><body>This is an example page content.<a href=\"http://localhost/confluence/next.html\">Click here!</a></body></html>";
		String prefix = ".txt";
		String suffix = "page-01";
		
		File testFile = File.createTempFile(prefix, suffix);
		
		beautifier.beautify(htmlContent, testFile);
		
		assertFalse(parser.getFeature("http://xml.org/sax/features/namespaces"));
		assertFalse(parser.getFeature("http://cyberneko.org/html/features/balance-tags"));
		assertTrue(parser.getFeature("http://cyberneko.org/html/features/scanner/cdata-sections"));
		assertTrue(parser.getFeature("http://apache.org/xml/features/scanner/notify-char-refs"));
		assertTrue(parser.getFeature("http://apache.org/xml/features/scanner/notify-builtin-refs"));
		assertTrue(parser.getFeature("http://cyberneko.org/html/features/scanner/notify-builtin-refs"));
		assertTrue(parser.getFeature("http://cyberneko.org/html/features/scanner/fix-mswindows-refs"));
		assertTrue(parser.getFeature("http://cyberneko.org/html/features/scanner/ignore-specified-charset"));
		assertFalse(parser.getFeature("http://cyberneko.org/html/features/report-errors"));
		
		XMLDocumentFilter[] filter = (XMLDocumentFilter[]) parser.getProperty("http://cyberneko.org/html/properties/filters");
		assertNotNull(filter);
		
		String fileContent = FileUtils.readFileToString(testFile);
		
		String expectedContent = htmlContent.replaceAll("html>", "HTML>").replaceAll("body>", "BODY>").replaceAll("<a", "<A").replaceAll("a>", "A>");
		
		assertEquals(expectedContent, fileContent);
		
		assertTrue(testFile.delete());
	}
}
