package com.it.could.confluence.autoexport.actions;

import java.util.Arrays;

import it.could.confluence.autoexport.ConfigurationManager;
import it.could.confluence.autoexport.ExportManager;
import it.could.confluence.autoexport.actions.RebuildAction;
import junit.framework.TestCase;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceType;
import com.opensymphony.xwork.Action;

public class RebuildActionTestCase extends TestCase
{
	private RebuildAction rebuildAction;
	
	@Mock private ExportManager exportManager;
	@Mock private SpaceManager spaceManager;
	@Mock private ConfigurationManager configurationManager;
	
	private Space demoSpace;
	private String spaceKey = "ds";
	private String spaceName = "Demostration Space";
	private String spaces[];
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		demoSpace = new Space();
		demoSpace.setName(spaceName);
		demoSpace.setKey(spaceKey);
		
		rebuildAction = new RebuildAction();
		rebuildAction.setExportManager(exportManager);
		rebuildAction.setSpaceManager(spaceManager);
		rebuildAction.setConfigurationManager(configurationManager);
	}

	public void testExportAllSpacesWithOneSpaceAndNoPreviousExecutor()
	{
		String spaces[] = {"*"};
		
		rebuildAction.setSpaces(spaces);
		
		when(spaceManager.getAllSpaces()).thenReturn(Arrays.asList(demoSpace));
		
		
		assertEquals(Action.SUCCESS, rebuildAction.execute());
		assertTrue(rebuildAction.hasActionMessages());
		assertTrue(rebuildAction.getActionMessages().contains(rebuildAction.getText("msg.started")));
		assertFalse(rebuildAction.hasActionErrors());
	}
	
	public void testExportOneSpaceWithPreviousExecutor()
	{
		String spaces[] = {"ds"};
		
		rebuildAction.setSpaces(spaces);
		
		when(spaceManager.getSpace(spaces[0])).thenReturn(demoSpace);
		
		assertEquals(Action.SUCCESS, rebuildAction.execute());
		assertFalse(rebuildAction.hasActionMessages());
		assertTrue(rebuildAction.hasActionErrors());
		assertTrue(rebuildAction.getActionErrors().contains(rebuildAction.getText("err.running")));
	}
}
