package com.it.could.confluence.autoexport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.confluence.core.ContentEntityManager;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.xml.sax.SAXException;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyString;

import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.actions.ViewPageAction;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.renderer.WikiStyleRenderer;

import junit.framework.TestCase;
import it.could.confluence.autoexport.ConfigurationManager;
import it.could.confluence.autoexport.ExportManager;
import it.could.confluence.autoexport.LocationManager;
import it.could.confluence.autoexport.TemplatesManager;
import it.could.confluence.autoexport.engine.Notifiable;
import it.could.confluence.localization.LocalizedException;
import it.could.util.location.Location;

public class ExportManagerTestCase extends TestCase
{
	private ExportManager exportManager;

	@Mock private TemplatesManager templatesManager;
	@Mock private LocationManager locationManager;
	@Mock private ConfigurationManager configurationManager;
	@Mock private SpaceManager spaceManager;
	@Mock private PageManager pageManager;
    @Mock private ContentEntityManager contentEntityManager;
	@Mock private ThumbnailManager thumbnailManager;
	@Mock private WikiStyleRenderer wikiStyleRenderer;
	@Mock private PluginAccessor pluginAccessor;
	@Mock private XhtmlContent xhtmlContent;

	@Mock private Notifiable notifiable;
	@Mock private File styleFile;
	@Mock private File resourceDir;
	@Mock private FileOutputStream stream;
	@Mock private VelocityContext context;
	@Mock private Template template;
	@Mock private PageContext pageContext;
	@Mock private Location location;
	@Mock private File pageFile;
	@Mock private File spaceDir;

	private Space demospace;
	
	private String styleData = "";
	private String spaceKey = "ds";
	private String spaceKeys[];
	private String confluenceUrl = "http://localhost:1990/confluence";
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		demospace = new Space();
		demospace.setKey(spaceKey);
		
		spaceKeys = new String[1];
		spaceKeys[0] = spaceKey;
		
		when(spaceManager.getSpace(spaceKey)).thenReturn(demospace);
		when(locationManager.exportable(demospace)).thenReturn(true);
		when(locationManager.getFile(demospace, "space.css")).thenReturn(styleFile);
		when(styleFile.getParentFile()).thenReturn(resourceDir);
		when(resourceDir.isDirectory()).thenReturn(false);
		
		exportManager = new TestExportManager();
	}
	
	public void testExportSpaceWithoutPages()
	{
		exportManager = new TestExportManager()
		{
			@Override
			public void export(Space space, Notifiable notifiable,
					boolean exportPages) 
			{
				assertEquals(demospace, space);
				super.export(space, notifiable, exportPages);
			}
		};
		
		exportManager.export(spaceKeys, notifiable, false);
	}
	
	public void testExportSpaceWithPages() throws LocalizedException
	{
		String pageContent = "This is a demo page.";
		String body = "<body>This is the body tag</body>";
		String styleUri = "/autoexport/space.css";
		
		final Page newPage = new Page();
		newPage.setSpace(demospace);
		newPage.setBodyAsString(pageContent);
		
		List<Page> pagesList = new ArrayList<Page>();
		pagesList.add(newPage);
		
		exportManager = new TestExportManager()
		{
			@Override
			public void export(AbstractPage page, Notifiable notifiable) 
			{
				assertEquals(newPage, page);
				super.export(page, notifiable);
			}
		};
		
		when(pageManager.getPages(demospace, true)).thenReturn(pagesList);
		when(locationManager.exportable(newPage)).thenReturn(true);
		when(templatesManager.getTemplate(spaceKey)).thenReturn(template);
		when(wikiStyleRenderer.convertWikiToXHtml(pageContext, pageContent)).thenReturn(body);
		when(locationManager.getLocation(demospace, "space.css")).thenReturn(location);
		when(location.toString()).thenReturn(styleUri);
		when(configurationManager.getConfluenceUrl()).thenReturn(confluenceUrl);
		when(locationManager.getFile(newPage)).thenReturn(pageFile);
		when(pageFile.getParentFile()).thenReturn(spaceDir);
		when(pageFile.isDirectory()).thenReturn(false);
		
		exportManager.export(spaceKeys, notifiable, true);
	}
	
	public void testExportSpaceWithoutSpaceKeys()
	{
		String[] noSpaceKeys = null;
		
		exportManager.export(noSpaceKeys, notifiable, false);
		
		verify(spaceManager, never()).getSpace(anyString());
	}
	
	public void testExportSpaceWithoutSpaceKey()
	{
		String noSpaceKey = null;
		
		exportManager.export(noSpaceKey, notifiable, false);
		
		verify(spaceManager, never()).getSpace(anyString());
	}
	
	public void testExportAllContentWithoutSpace()
	{
		Space noSpace = null;
		
		exportManager.export(noSpace, notifiable, false);
		
		verify(locationManager, never()).exportable(anyObject());
	}
	
	public void testExportNullPage()
	{
		exportManager.export((Page) null, notifiable);
		
		verify(locationManager, never()).exportable(anyObject());
	}
	
	public class TestExportManager extends ExportManager
	{
		public TestExportManager() {
			super(templatesManager, locationManager, configurationManager, spaceManager,
					pageManager, contentEntityManager, thumbnailManager, wikiStyleRenderer, pluginAccessor, xhtmlContent);
		}

		@Override
		protected String getRenderedSpaceStylesheet(Space space) {
			return styleData;
		}

		@Override
		protected FileOutputStream getFileOutputStream(File styleFile)
				throws FileNotFoundException {
			return stream;
		}

		@Override
		protected VelocityContext getVelocityContext() {
			return context;
		}

		@Override
		protected void autowireComponent(ViewPageAction action) {
			// Do nothing
		}

		@Override
		protected void getBeautifier(AbstractPage page, File pageFile,
				StringWriter writer) throws SAXException, IOException {
			// Do nothing
		}
	}
}
