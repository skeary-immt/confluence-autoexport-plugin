package com.it.could.confluence.autoexport;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;

import com.atlassian.config.ApplicationConfig;
import com.atlassian.config.ApplicationConfiguration;
import com.atlassian.config.ConfigurationException;
import com.atlassian.config.bootstrap.AtlassianBootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.user.EntityException;
import com.atlassian.user.User;
import com.atlassian.user.UserManager;
import com.atlassian.user.impl.DefaultUser;

import it.could.confluence.autoexport.ConfigurationManager;
import it.could.confluence.localization.LocalizedException;
import junit.framework.TestCase;

public class ConfigurationManagerTestCase extends TestCase
{
	private ConfigurationManager configurationManager;

	@Mock private UserManager userManager;
	@Mock private SpaceManager spaceManager;
	@Mock private AtlassianBootstrapManager bootstrapManager;
	@Mock private SettingsManager settingsManager;
	@Mock private Iterator iterator;
	@Mock private ApplicationConfiguration mockApplicationConfig;
	
	private ApplicationConfiguration applicationConfig;
	
	private boolean booleanProperty = true;
	
	private String encoding = "UTF-8";
	private String rootPath = "export";
	private String userName = "admin";
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		applicationConfig = new ApplicationConfig()
		{
			@Override
			public void save() throws ConfigurationException {
				// Do nothing
			}

			@Override
			public boolean getBooleanProperty(Object key) {
				return booleanProperty;
			}
		};
		
		configurationManager = new ConfigurationManager(userManager, spaceManager, bootstrapManager, settingsManager, applicationConfig);
	}
	
	public void testSaveConfigurationWithoutAnySettings() throws LocalizedException
	{
		configurationManager.save();
		
		assertNull(applicationConfig.getProperty(ConfigurationManager.ENCODING));
		assertNull(applicationConfig.getProperty(ConfigurationManager.ROOT_PATH));
		assertNull(applicationConfig.getProperty(ConfigurationManager.USER_NAME));
		assertTrue((Boolean)applicationConfig.getProperty(ConfigurationManager.CONFIGURED));
	}
	
	public void testSaveConfigurationWithSettings() throws LocalizedException
	{
		configurationManager.setEncoding(encoding);
		configurationManager.setRootPath(rootPath);
		configurationManager.setUserName(userName);
		
		configurationManager.save();
		
		assertEquals(encoding, applicationConfig.getProperty(ConfigurationManager.ENCODING));
		assertEquals(rootPath, applicationConfig.getProperty(ConfigurationManager.ROOT_PATH));
		assertEquals(userName, applicationConfig.getProperty(ConfigurationManager.USER_NAME));
		assertTrue((Boolean)applicationConfig.getProperty(ConfigurationManager.CONFIGURED));
	}
	
	public void testDeleteAllTheConfigurationAndRemoveThePropertiesFile() throws LocalizedException, ConfigurationException
	{
		configurationManager = new ConfigurationManager(userManager, spaceManager, bootstrapManager, settingsManager, mockApplicationConfig);
		
		String key = "1";
		
		Map<String, Iterator> map = new HashMap<String, Iterator>();
		map.put(key, iterator);
		
		when(mockApplicationConfig.getPropertiesWithPrefix(ConfigurationManager.PREFIX)).thenReturn(map);
		when(mockApplicationConfig.getProperty(ConfigurationManager.ENCODING)).thenReturn(encoding);
		when(mockApplicationConfig.getProperty(ConfigurationManager.ROOT_PATH)).thenReturn(rootPath);
		when(mockApplicationConfig.getProperty(ConfigurationManager.USER_NAME)).thenReturn(userName);
		
		configurationManager.setEncoding(encoding);
		configurationManager.setRootPath(rootPath);
		configurationManager.setUserName(userName);
		configurationManager.delete();
		
		assertEquals(encoding, configurationManager.getEncoding());
		assertEquals(rootPath, configurationManager.getRootPath());
		assertEquals(userName, configurationManager.getUserName());
		
		verify(mockApplicationConfig).removeProperty(key);
		verify(mockApplicationConfig, atLeastOnce()).save();
	}
	
	public void testValidateRootPathWhichIsADirectory() throws LocalizedException
	{
		File testFile = new File(ConfigurationManager.ROOT_PATH);
		
		try
		{
			assertTrue(testFile.mkdir());
			
			String rootPath = testFile.getAbsolutePath();
			
			assertEquals(testFile.getAbsolutePath(), configurationManager.validateRootPath(rootPath));
		}
		finally
		{
			assertTrue(testFile.delete());
		}
	}
	
	public void testValidateLoginUser() throws LocalizedException, EntityException
	{
		String name = "Administrator";
		
		User user = new DefaultUser(name);
		
		when(userManager.getUser(userName)).thenReturn(user);
		
		assertEquals(name, configurationManager.validateUserName(userName));
	}
	
	public void testUnknownUserNameExceptionThrownWhenNoUserIsFound()
	{
		try {
			configurationManager.validateUserName(userName);
		} catch (LocalizedException e) {
			assertEquals("Unable to resolve user \"" + userName + "\"", e.getLocalizedMessage());
		}
	}
	
	public void testValidateAnonymousUser() throws LocalizedException
	{
		String userName = "";
		
		assertNull(configurationManager.validateUserName(userName));
	}
	
	public void testValidateExistingHomeSpace() throws LocalizedException
	{
		String homeSpace = "Home Space";
		String spaceKey = "ds";
		
		Space demoSpace = new Space();
		demoSpace.setKey(spaceKey);
		
		when(spaceManager.getSpace(homeSpace)).thenReturn(demoSpace);
		
		assertEquals(spaceKey, configurationManager.validateHomeSpace(homeSpace));
	}
	
	public void testLocalizedExceptionThrowedWhenValidatingHomeSpaceWithNoSpace()
	{
		String homeSpace = "MySpace";
		
		try {
			configurationManager.validateHomeSpace(homeSpace);
		} catch (LocalizedException e) {
			assertEquals("Home space \"" + homeSpace + "\" unknown", e.getLocalizedMessage());
		}
	}
	
	public void testValidateNullHomeSpace() throws LocalizedException
	{
		assertNull(configurationManager.validateHomeSpace(""));
	}
	
	public void testExceptionThrownWhenInvalidEncodingHasBeenGiven()
	{
		String invalidEncoding = "12345";
		
		try {
			configurationManager.validateEncoding(invalidEncoding);
		} catch (LocalizedException e) {
			assertEquals("Unsupported encoding \"" + invalidEncoding + "\"", e.getLocalizedMessage());
		}
	}
	
	public void testWhenThePluginHasBeenProperlyConfigured() throws LocalizedException, EntityException, IOException
	{
		final File rootFile = File.createTempFile("root", ".txt");
		final File rootDir = rootFile.getParentFile();
		
		configurationManager = new ConfigurationManager(userManager, spaceManager, bootstrapManager, settingsManager, applicationConfig)
		{
			@Override
			protected File getRootFile(String rootPath) 
			{
				return rootDir;
			}
		};
		
		booleanProperty = true;
		
		String name = "Administrator";
		
		User user = new DefaultUser(name);
		
		configurationManager.setEncoding(encoding);
		configurationManager.setRootPath(rootPath);
		configurationManager.setUserName(userName);
		
		when(userManager.getUser(userName)).thenReturn(user);
		
		assertTrue(configurationManager.isConfigured());
		
		assertTrue(rootFile.delete());
	}
	
	public void testGetExtension()
	{
		assertEquals(".html", configurationManager.getExtension());
	}
}
