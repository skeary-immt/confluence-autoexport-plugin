package com.it.could.confluence.autoexport.engine;

import it.could.confluence.autoexport.ConfigurationManager;
import it.could.confluence.autoexport.engine.ExportUtils;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import com.opensymphony.util.TextUtils;

import junit.framework.TestCase;

public class ExportUtilsTestCase extends TestCase
{
	private ExportUtils exportUtils;
	
	private String separator = ">";
	
	@Mock private ConfigurationManager configurationManager;
	@Mock private PluginAccessor pluginAccessor;
	@Mock private Plugin plugin;
	@Mock private PluginInformation pluginInformation;
	
	private Space demoSpace;
	private String spaceName = "Demostration Space";
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		demoSpace = new Space();
		demoSpace.setName(spaceName);
		
		exportUtils = new ExportUtils(configurationManager, pluginAccessor);
	}
	
	public void testExportBreadcrumbsPageLink()
	{
		final String urlPath = "http://localhost:1990/confluence";
		
		Page parentPage = new Page();
		parentPage.setSpace(demoSpace);
		
		demoSpace.setHomePage(parentPage);
		
		when(configurationManager.getConfluenceUrl()).thenReturn(urlPath);
		
		String expectedResult = "<a href=\""+urlPath+"/pages/viewpage.action?pageId=0\" title=\""+spaceName+"\">"+
								spaceName+"</a>&nbsp;" + TextUtils.htmlEncode(separator) + "&nbsp;"+
								"<a href=\""+urlPath+"/pages/viewpage.action?pageId=0\" title=\"\"></a>";
		
		assertEquals(expectedResult, exportUtils.breadcrumbs(parentPage));
	}
	
	public void testGetConfluenceInfo()
	{
		String expectedConfluenceInfo = "<a href=\"http://www.atlassian.com/confluence/\">"+
										"Atlassian Confluence</a> (Version: " +
										GeneralUtil.getVersionNumber()+
										" Build: "+
										GeneralUtil.getBuildNumber()+
										" "+
										GeneralUtil.getBuildDateString()+
										")";
		
		assertEquals(expectedConfluenceInfo, exportUtils.getConfluenceInfo());
	}
	
	public void testGetAutoExportPluginInfo()
	{
		String pluginName = "Auto Export Plugin";
		String pluginVersion = "1.0-beta2";
		String pluginVendorUrl = "www.atlassian.com";
		
		String expectedInfo = "<a href=\""+pluginVendorUrl+"\">"+pluginName+"</a> (Version: "+pluginVersion+")";
		
		when(pluginAccessor.getPlugin("confluence.extra.autoexport")).thenReturn(plugin);
		when(plugin.getName()).thenReturn(pluginName);
		when(plugin.getPluginInformation()).thenReturn(pluginInformation);
		when(pluginInformation.getVersion()).thenReturn(pluginVersion);
		when(pluginInformation.getVendorUrl()).thenReturn(pluginVendorUrl);
		
		assertEquals(expectedInfo, exportUtils.getAutoexportInfo());
	}
}
