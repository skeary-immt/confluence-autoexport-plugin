package com.it.could.confluence.autoexport.actions;

import java.io.IOException;

import it.could.confluence.autoexport.TemplatesManager;
import it.could.confluence.autoexport.actions.TemplateAction;
import it.could.confluence.localization.LocalizedException;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.atLeastOnce;

import com.atlassian.confluence.spaces.SpaceManager;
import com.opensymphony.xwork.Action;

import junit.framework.TestCase;

public class TemplateActionTestCase extends TestCase
{
	private TemplateAction templateAction;
	
	private String space = "Demostration Space";
	
	@Mock private TemplatesManager templatesManager;
	@Mock private SpaceManager spaceManager;
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		templateAction = new TemplateAction();
		templateAction.setTemplatesManager(templatesManager);
		templateAction.setSpaceManager(spaceManager);
		templateAction.setSpace(space);
	}
	
	public void testSaveTemplateWithoutAnyData() throws IOException, LocalizedException
	{
		assertEquals(Action.SUCCESS, templateAction.save());
		assertFalse(templateAction.hasActionMessages());
		assertTrue(templateAction.hasActionErrors());
		assertTrue(templateAction.getActionErrors().contains(templateAction.getText("err.nodata")));
		
		verify(templatesManager, never()).writeCustomTemplate(anyString(), anyString());
	}
	
	public void testSave() throws IOException, LocalizedException
	{
		String defaultTemplate = "<html>Default Template</html>";
		String customTemplate = "<html>Custom Template</html>";
		
		when(templatesManager.readDefaultTemplate()).thenReturn(defaultTemplate);
		when(templatesManager.readCustomTemplate(space)).thenReturn(customTemplate);
		
		assertEquals(Action.SUCCESS, templateAction.execute());
		assertEquals(Action.SUCCESS, templateAction.save());
		assertTrue(templateAction.hasActionMessages());
		assertTrue(templateAction.getActionMessages().contains(templateAction.getText("msg.saved")));
		
		verify(templatesManager, atLeastOnce()).writeCustomTemplate(space, defaultTemplate);
	}
	
	public void testRestoreToDefaultTemplate() throws IOException
	{
		when(templatesManager.removeCustomTemplate(space)).thenReturn(true);
		
		assertEquals(Action.SUCCESS, templateAction.restore());
		
		assertTrue(templateAction.hasActionMessages());
		assertTrue(templateAction.getActionMessages().contains(templateAction.getText("msg.deleted")));
	}
	
	public void testReadAndDisplayTheCurrentTemplate() throws LocalizedException
	{
		String defaultTemplate = "<html><body>Default Template</body></html>";
		
		when(templatesManager.readDefaultTemplate()).thenReturn(defaultTemplate);
		
		assertEquals(Action.SUCCESS, templateAction.read());
		
		assertEquals(defaultTemplate, templateAction.getData());
	}
}
