package com.it.could.confluence.autoexport.engine;

import java.util.Arrays;
import java.util.List;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.eq;

import com.atlassian.confluence.event.events.content.blogpost.BlogPostCreateEvent;
import com.atlassian.confluence.event.events.content.blogpost.BlogPostRemoveEvent;
import com.atlassian.confluence.event.events.content.blogpost.BlogPostUpdateEvent;
import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageRemoveEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;

import it.could.confluence.autoexport.ExportManager;
import it.could.confluence.autoexport.engine.ExportListener;
import it.could.confluence.autoexport.engine.Notifiable;
import junit.framework.TestCase;

public class ExportListenerTestCase extends TestCase
{
	private ExportListener exportListener;

	@Mock private ExportManager exportManager;
	@Mock private PageCreateEvent pageCreateEvent;
	@Mock private PageUpdateEvent pageUpdateEvent;
	@Mock private BlogPostCreateEvent blogPostCreateEvent;
	
	private Page demoPage;
	private Space demoSpace;
	
	private String spaceKey = "ds";
	private Long pageId = new Long(100);
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		demoSpace = new Space();
		demoSpace.setKey(spaceKey);
		
		demoPage = new Page();
		demoPage.setId(pageId);
		demoPage.setSpace(demoSpace);
		
		exportListener = new ExportListener(exportManager);
	}
	
	public void testHandlePageCreateEvent()
	{
		when(pageCreateEvent.getPage()).thenReturn(demoPage);
		
		exportListener.handleEvent(pageCreateEvent);
		
		verifyExportManagerIsExecuted(demoPage, demoSpace);
	}

	public void testHandlePageUpdateEvent()
	{
		String newSpaceKey = "tst";
		
		Long newPageId = new Long(200);
		
		Space newSpace = new Space();
		newSpace.setKey(newSpaceKey);
		
		Page newPage = new Page();
		newPage.setSpace(newSpace);
		newPage.setId(newPageId);
		
		when(pageUpdateEvent.getOriginalPage()).thenReturn(demoPage);
		when(pageUpdateEvent.getPage()).thenReturn(newPage);
		
		exportListener.handleEvent(pageUpdateEvent);
		
		verifyExportManagerIsExecuted(newPage, newSpace);
	}
	
	public void testHandleBlogPostUpdateEvent()
	{
		BlogPost blogPost = new BlogPost();
		blogPost.setSpace(demoSpace);
		
		when(blogPostCreateEvent.getBlogPost()).thenReturn(blogPost);
		
		exportListener.handleEvent(blogPostCreateEvent);
		
		verifyExportManagerIsExecuted(blogPost, demoSpace);
	}
	
	public void testGetHandledEventClasses()
	{
		Class[] handledEventClasses = exportListener.getHandledEventClasses();
		
		assertTrue(handledEventClasses.length > 0);
		
		List result = Arrays.asList(exportListener.getHandledEventClasses());
		
		assertTrue(result.contains(PageCreateEvent.class));
		assertTrue(result.contains(PageRemoveEvent.class));
		assertTrue(result.contains(PageUpdateEvent.class));
		assertTrue(result.contains(BlogPostCreateEvent.class));
		assertTrue(result.contains(BlogPostRemoveEvent.class));
		assertTrue(result.contains(BlogPostUpdateEvent.class));
	}
	
	private void verifyExportManagerIsExecuted(AbstractPage page, Space space)
	{
		verify(exportManager, atLeastOnce()).export(eq(page), (Notifiable) anyObject());
		verify(exportManager, atLeastOnce()).export(eq(space), (Notifiable) anyObject(), eq(false));
	}
}
