package com.it.could.confluence.autoexport;

import java.io.File;
import java.io.IOException;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.user.EntityException;
import com.atlassian.user.User;
import com.atlassian.user.UserManager;

import it.could.confluence.autoexport.ConfigurationManager;
import it.could.confluence.autoexport.LocationManager;
import it.could.confluence.localization.LocalizedException;
import junit.framework.TestCase;

public class LocationManagerTestCase extends TestCase
{
	private LocationManager locationManager;

	@Mock private ConfigurationManager configurationManager;
	@Mock private PermissionManager permissionManager;
	@Mock private UserManager userManager;
	
	@Mock private User user;
	
	private String confluenceUrl = "http://localhost:1990/confluence";
	private String extension = ".txt";
	private String rootPath = "\\display\\Theme";
	private String spaceKey = "ds";
	private String pageTitle = "SecretDocument";
	private long pageId = 100;
	
	private Space demoSpace;
	private Page demoPage;
	
	private File resultFile;
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		demoSpace = new Space();
		demoSpace.setKey(spaceKey);
		
		demoPage = new Page();
		demoPage.setSpace(demoSpace);
		demoPage.setTitle(pageTitle);
		demoPage.setId(pageId);
		
		when(configurationManager.getExtension()).thenReturn(extension);
		when(configurationManager.getRootPath()).thenReturn(rootPath);
		
		locationManager = new LocationManager(configurationManager, permissionManager, userManager);
	}
	
	public void testGetAttachmentFromPage() throws IOException
	{
		resultFile = locationManager.getFile(demoPage);
		
		assertEquals(pageTitle.toLowerCase() + extension, resultFile.getName());
		assertEquals(spaceKey, resultFile.getParentFile().getName());
	}

	public void testGetAttachmentFromSpace()
	{
		String resource = "test.txt";
		
		resultFile = locationManager.getFile(demoSpace, resource);
		
		assertEquals(resource, resultFile.getName());
		assertEquals("resources", resultFile.getParentFile().getName());
		assertEquals(spaceKey, resultFile.getParentFile().getParentFile().getName());
	}
	
	public void testGetAttachmentFileLocation()
	{
		String attachmentFileName = "secret.txt";
		
		Attachment attachment = new Attachment();
		attachment.setFileName(attachmentFileName);
		attachment.setContent(demoPage);
		boolean thumbnail = false;
		
		resultFile = locationManager.getFile(attachment, thumbnail);
		
		assertEquals(attachmentFileName, resultFile.getName());
	}
	
	public void testGetPageLocationIfPageIsNotExportable()
	{
		when(configurationManager.getConfluenceUrl()).thenReturn(confluenceUrl);
		
		String expectedResult = confluenceUrl + "/display/"+spaceKey +"/"+pageTitle;
		
		assertEquals(expectedResult, locationManager.getLocation(demoPage).toString());
	}
	
	public void testGetExportablePageLocation() throws EntityException, LocalizedException
	{
		String userName = "admin";
		
		when(configurationManager.getConfluenceUrl()).thenReturn(confluenceUrl);
		when(configurationManager.isConfigured()).thenReturn(true);
		when(configurationManager.getUserName()).thenReturn(userName);
		when(userManager.getUser(userName)).thenReturn(user);
		when(permissionManager.hasPermission(user, Permission.VIEW, demoPage)).thenReturn(true);
		when(configurationManager.getExtension()).thenReturn(extension);
		
		String expectedResult = "autoexport:///" + spaceKey + "/" + pageTitle.toLowerCase() + extension;
		
		assertEquals(expectedResult, locationManager.getLocation(demoPage).toString());
	}
	
	public void testGetExportableSpaceLocation()
	{
		String resource = "test.txt";
		
		String expectedResult = "autoexport:///" + spaceKey + "/resources/" + resource;
		
		assertEquals(expectedResult, locationManager.getLocation(demoSpace, resource).toString());
	}
	
	public void testGetNotExportableAttachmentLocation()
	{
		final String urlPath = "/images/pic01.jpg";
		
		Attachment attachment = new Attachment()
		{
			@Override
			public String getUrlPath() 
			{
				return urlPath;
			}
		};
		boolean thumbnail = false;
		
		when(configurationManager.getConfluenceUrl()).thenReturn(confluenceUrl);
		
		String expectedResult = confluenceUrl + urlPath;
		
		assertEquals(expectedResult, locationManager.getLocation(attachment, thumbnail).toString());
	}
	
	public void testGetExportableAttachmentLocationWithThumbNail() throws EntityException, LocalizedException
	{
		String userName = "admin";
		String attachmentFileName = "pic01";
		
		Attachment attachment = new Attachment();
		attachment.setFileName(attachmentFileName);
		attachment.setContent(demoPage);
		boolean thumbnail = true;
		
		when(configurationManager.isConfigured()).thenReturn(true);
		when(configurationManager.getUserName()).thenReturn(userName);
		when(userManager.getUser(userName)).thenReturn(user);
		when(permissionManager.hasPermission(user, Permission.VIEW, attachment)).thenReturn(true);
		
		String expectedResult = "autoexport:///" + spaceKey + "/" + pageTitle.toLowerCase() + ".data/" + attachmentFileName + ".jpeg";
		
		assertEquals(expectedResult, locationManager.getLocation(attachment, thumbnail).toString());
	}
}
