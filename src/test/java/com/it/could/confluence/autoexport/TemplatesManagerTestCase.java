package com.it.could.confluence.autoexport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.net.URL;

import it.could.confluence.autoexport.ConfigurationManager;
import it.could.confluence.autoexport.PluginBuilder;
import it.could.confluence.autoexport.TemplatesManager;
import it.could.confluence.localization.LocalizedException;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;

import com.atlassian.plugin.PluginAccessor;
import com.opensymphony.webwork.views.velocity.VelocityManager;

import junit.framework.TestCase;

public class TemplatesManagerTestCase extends TestCase
{
	private TemplatesManager templatesManager;
	
	@Mock private ConfigurationManager configurationManager;
	@Mock private PluginBuilder pluginBuilder;
	@Mock private PluginAccessor pluginAccessor;
	
	@Mock private ClassLoader classLoader;
	@Mock private VelocityManager velocityManager;
	@Mock private VelocityEngine velocityEngine;
	
	@Mock private Template template;
	
	private String spaceKey = "ds";
	private String templateName;
	private URL urlTemplate;
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		templateName = "autoexport." + spaceKey + ".vm";
		urlTemplate = new URL("http://localhost:1990/confluence");
		
		when(pluginAccessor.getClassLoader()).thenReturn(classLoader);
		when(classLoader.getResource(templateName)).thenReturn(urlTemplate);
		when(velocityManager.getVelocityEngine()).thenReturn(velocityEngine);
		when(velocityEngine.getTemplate(templateName, TemplatesManager.ENCODING)).thenReturn(template);
		
		templatesManager = new TemplatesManager(configurationManager, pluginBuilder, pluginAccessor)
		{
			@Override
			protected VelocityManager getVelocityManager() 
			{
				return velocityManager;
			}

			@Override
			protected ClassLoader fetchClassLoader() 
			{
				return classLoader;
			}
		};
	}
	
	public void testGetDefaultTemplateFromASpace() throws ResourceNotFoundException, ParseErrorException, Exception
	{
		assertEquals(template, templatesManager.getTemplate(spaceKey));
	}
	
	public void testWriteTheContentsAsCustomTemplate() throws LocalizedException
	{
		String template = "";
		
		templatesManager.writeCustomTemplate(spaceKey, template);
		
		verify(pluginBuilder, atLeastOnce()).add(templateName, template);
	}
	
	public void testReadCustomTemplateWithoutUrl() throws LocalizedException, IOException
	{
		String confluenceHome = "../target";
		String prefix = ".txt";
		String suffix = "testReadCustomTemplateWithoutUrl";
		
		String fileContent = "<html>\"U & I\"</html>";
		
		File testFile = null;
		FileOutputStream outputStream = null;
		InputStream inputStream = null;
		
		try
		{
			testFile = File.createTempFile(prefix, suffix);
			
			outputStream = new FileOutputStream(testFile.getPath());
			outputStream.write(fileContent.getBytes());
			outputStream.flush();
			
			inputStream = new FileInputStream(testFile.getPath());
			
			when(classLoader.getResource(templateName)).thenReturn(null);
			when(configurationManager.getConfluenceHome()).thenReturn(confluenceHome);
			when(classLoader.getResourceAsStream(TemplatesManager.DEFAULT_TEMPLATE)).thenReturn(inputStream);
			
			String expectedResult = fileContent.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;")
										.replaceAll("\"", "&quot;");
			
			assertEquals(expectedResult, templatesManager.readCustomTemplate(spaceKey));
			
			
		}
		finally
		{
			outputStream.close();
			inputStream.close();
			
			assertTrue(testFile.delete());
		}
	}
	
	public void testExceptionThrownWhenNoTemplateIsFound() throws ResourceNotFoundException, ParseErrorException, Exception
	{
		templatesManager = new TemplatesManager(configurationManager, pluginBuilder, pluginAccessor)
		{
			@Override
			public boolean hasCustomTemplate(String spaceKey) 
			{
				return null != spaceKey? false:true;
			}

			@Override
			protected VelocityManager getVelocityManager() 
			{
				return velocityManager;
			}
		};
		
		String templateString = "autoexport.vm";
		
		when(velocityEngine.getTemplate(templateString, TemplatesManager.ENCODING)).thenThrow(new ResourceNotFoundException(templateString + "not found"));
		
		try {
			templatesManager.getTemplate("");
		} catch (LocalizedException e) {
			assertEquals("template.notfound", e.getLocalizedMessage());
		}
	}
}
