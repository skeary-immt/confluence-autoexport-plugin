package com.it.could.confluence.autoexport.actions;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.spaces.SpaceManager;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;

import it.could.confluence.autoexport.ConfigurationManager;
import it.could.confluence.autoexport.TemplatesManager;
import it.could.confluence.autoexport.actions.ConfigurationAction;
import it.could.confluence.localization.LocalizedException;
import junit.framework.TestCase;

public class ConfigurationActionTestCase extends TestCase
{
	private ConfigurationAction configurationAction;

	@Mock private SpaceManager spaceManager;
	@Mock private ConfigurationManager configurationManager;
	@Mock private TemplatesManager templatesManager;
	
	private String encoding = "UTF-8";
	private String rootPath = "localhost/confluence";
	private String userName = "admin";
	
	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		configurationAction = new ConfigurationAction();
		configurationAction.setSpaceManager(spaceManager);
		configurationAction.setConfigurationManager(configurationManager);
		configurationAction.setTemplatesManager(templatesManager);
	}
	
	public void testExecuteMethodWhileConfigurationIsNotConfigured() throws LocalizedException
	{
		when(configurationManager.getEncoding()).thenReturn(encoding);
		when(configurationManager.getRootPath()).thenReturn(rootPath);
		when(configurationManager.getUserName()).thenReturn(userName);
		when(configurationManager.validateEncoding(encoding)).thenReturn(encoding);
		when(configurationManager.validateRootPath(rootPath)).thenReturn(rootPath);
		when(configurationManager.validateUserName(userName)).thenReturn(userName);
		
		assertEquals(ConfigurationAction.SUCCESS ,configurationAction.execute());
		
		assertEquals(encoding, configurationAction.getEncoding());
		assertEquals(rootPath, configurationAction.getRootPath());
		assertEquals(userName, configurationAction.getUserName());
		assertEquals("AutoExport Plugin not yet configured.", configurationAction.getActionErrors().toArray()[0]);
	}
	
	public void testSaveConfiguration() throws LocalizedException
	{
		String actionMessage = "Configuration saved succesfully";
		
		when(configurationManager.getEncoding()).thenReturn(encoding);
		when(configurationManager.getRootPath()).thenReturn(rootPath);
		when(configurationManager.getUserName()).thenReturn(userName);
		
		assertEquals(ConfigurationAction.SUCCESS ,configurationAction.execute());
		
		assertEquals(ConfigurationAction.SUCCESS, configurationAction.configure());
		
		assertFalse(configurationAction.getActionMessages().isEmpty());
		
		assertEquals(actionMessage, configurationAction.getActionMessages().toArray()[0]);
		
		verify(configurationManager, atLeastOnce()).setEncoding(encoding);
		verify(configurationManager, atLeastOnce()).setRootPath(rootPath);
		verify(configurationManager, atLeastOnce()).setUserName(userName);
		
		verify(configurationManager, atLeastOnce()).save();
	}
	
	public void testSuccessfullyRemovePreviousConfiguration() throws LocalizedException
	{
		String actionMessage = "Configuration deleted successfully";
		
		assertEquals(ConfigurationAction.SUCCESS, configurationAction.unconfigure());
		
		assertFalse(configurationAction.getActionMessages().isEmpty());
		assertEquals(actionMessage, configurationAction.getActionMessages().toArray()[0]);
		
		verify(configurationManager, atLeastOnce()).delete();
	}
}
