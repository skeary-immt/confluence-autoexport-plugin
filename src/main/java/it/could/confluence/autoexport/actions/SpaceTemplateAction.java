package it.could.confluence.autoexport.actions;

import java.io.IOException;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.SpaceAware;

public class SpaceTemplateAction extends TemplateAction implements SpaceAware
{
    private Space space;

    /**
     * <p>Read and display the current template.</p>
     */
    public String read()
    {
        this.setSpace(space.getKey());
        return super.read();
    }

    /**
     * <p>Save the current template.</p>
     * @throws IOException 
     */
    public String save() throws IOException
    {
        this.setSpace(space.getKey());
        return super.save();
    }

    /**
     * <p>Delete the current template and restore the default one.</p>
     * @throws IOException 
     */
    public String restore() throws IOException
    {
        this.setSpace(space.getKey());
        return super.restore();
    }

    public boolean isSpaceRequired() 
    {
        return true;
    }

    public boolean isViewPermissionRequired() 
    {
        return true;
    }

    public void setSpace(Space space) 
    {
        this.space = space;
    }

    public Space getSpace() 
    {
        return space;
    }
}
