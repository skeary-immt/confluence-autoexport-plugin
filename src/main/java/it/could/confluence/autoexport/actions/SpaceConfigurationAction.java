package it.could.confluence.autoexport.actions;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.SpaceAware;

public class SpaceConfigurationAction extends ConfigurationAction implements SpaceAware
{
    private Space space;

    public boolean isViewPermissionRequired() 
    {
        return true;
    }

    public boolean isSpaceRequired() 
    {
        return true;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public Space getSpace() 
    {
        return space;
    }
}
