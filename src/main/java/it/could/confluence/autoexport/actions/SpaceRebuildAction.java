package it.could.confluence.autoexport.actions;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.SpaceAware;

public class SpaceRebuildAction extends RebuildAction implements SpaceAware
{
    private Space space;

    public boolean isSpaceRequired() 
    {
        return true;
    }

    public boolean isViewPermissionRequired() 
    {
        return true;
    }

    public void setSpace(Space space) 
    {
        this.space = space;
    }

    public Space getSpace() 
    {
        return space;
    }
}
