package it.could.confluence.autoexport.engine;

import com.atlassian.confluence.servlet.simpledisplay.ConvertedPath;
import com.atlassian.confluence.servlet.simpledisplay.DefaultVelocityEngineResolver;
import com.atlassian.confluence.servlet.simpledisplay.PathConverter;
import com.atlassian.confluence.servlet.simpledisplay.VelocityEngineResolver;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * A Path converter that converts friendly mail path url to a viewmail action.
 *
 * This converter expects the url to have exactly 3 parameters
 * as:
 *
 * <ul>
 * <li>Space Key</li>
 * <li>"mail" literal</li>
 * <li>id (numeric)</li>
 * </ul>
 */
public class MailPathConverter implements PathConverter
{
    protected VelocityEngineResolver resolver = new DefaultVelocityEngineResolver();

    public void setResolver(VelocityEngineResolver resolver)
    {
        this.resolver = resolver;
    }

    private Pattern pattern = Pattern.compile("^/?(\\p{Alnum}*)/mail/([\\d]*)/?");
    public static final String VIEW_MAIL = "/mail/archive/viewmail.action?key=${key}&id=${id}";

    public boolean handles(String simplePath)
    {
        return pattern.matcher(simplePath).matches();
    }

   public ConvertedPath getPath(String simplePath)
    {
        Matcher m = pattern.matcher(simplePath);

        String spaceKey = "";
        String mailId = "";

        if (m.matches())
        {
            spaceKey = m.group(1);
            mailId = m.group(2);
        }

        ConvertedPath convertedPath = new ConvertedPath(VIEW_MAIL, resolver);
        convertedPath.addParameter("key",spaceKey);
        convertedPath.addParameter("id",mailId);
        return convertedPath;
    }
}